from uuid import uuid4

import requests
from django.contrib.auth.models import User
from django.db import models
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.test import APIClient, APITestCase

from . import settings


class BaseModel(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid4, editable=False)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Pagination(PageNumberPagination):
    page_size = settings.REST_FRAMEWORK.get("PAGE_SIZE")
    no_page_param = "no_page"

    def paginate_queryset(self, queryset, request, view=None):
        if "no_page" in request.GET:
            self.page_size = 0
        elif "page_size" in request.GET:
            self.page_size = request.GET["page_size"]
        return super().paginate_queryset(queryset, request, view)

    def get_paginated_response(self, data):
        return Response(
            {
                "links": {
                    "next": self.get_next_link(),
                    "previous": self.get_previous_link(),
                },
                "count": self.page.paginator.count,
                "total_pages": self.page.paginator.num_pages,
                "results": data,
            }
        )


class BaseTest(APITestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_superuser('admin', 'admin@admin.com', 'admin123')
        self.client.force_authenticate(user=self.user)


def get_address_by_zip_code(zip_code: str) -> dict:
    url = f"https://viacep.com.br/ws/{zip_code}/json/"
    response = requests.get(url)
    if response.status_code == 200:
        data: dict = response.json()
        return {
            "address_zip_code": data.get("cep"),
            "address_address": data.get("logradouro"),
            "address_complement": data.get("complemento")
            if data.get("complemento") != ""
            else None,
            "address_district": data.get("bairro"),
            "address_city": data.get("localidade"),
            "address_state": data.get("uf"),
            "address_country": "Brazil",
        }
    raise Exception(response.text)