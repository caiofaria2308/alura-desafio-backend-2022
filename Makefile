SHELL:=/bin/bash
ARGS = $(filter-out $@,$(MAKECMDGOALS))
MAKEFLAGS += --silent
BASE_PATH=${PWD}
PYTHON_EXEC=python
DOCKER_COMPOSE_FILE=$(shell echo -f docker-compose.yml -f docker-compose.override.yml)
VENV_PATH=~/.venv/vertc-ops

ifeq ($(OS),Windows_NT)
    OSFLAG=WIN32
    ifeq ($(PROCESSOR_ARCHITEW6432),AMD64)
        OSFLAG=AMD64
    else
        ifeq ($(PROCESSOR_ARCHITECTURE),AMD64)
            OSFLAG=AMD64
        endif
        ifeq ($(PROCESSOR_ARCHITECTURE),x86)
            OSFLAG=IA32
        endif
    endif
else
    UNAME_S := $(shell uname -s | tr A-Z a-z)
    ifeq ($(UNAME_S),linux)
        OSFLAG=LINUX
    endif
    ifeq ($(UNAME_S),darwin)
        OSFLAG=OSX
    endif
    UNAME_P := $(shell uname -p)
    ifeq ($(UNAME_P),x86_64)
        OSFLAG=AMD64
    endif
    ifneq ($(filter %86,$(UNAME_P)),)
        OSFLAG=IA32
    endif
    ifneq ($(filter arm%,$(UNAME_P)),)
        OSFLAG=ARM
    endif
endif

include .env
export $(shell sed 's/=.*//' .env)

black:
	docker-compose exec web black */*.py


isort:
	docker-compose exec web isort */*.py

flake:
	docker-compose exec web flake8 */*


fix_permissions:
	sudo chown -R ${USER}:${USER} .
    
build:
	docker-compose up -d --remove-orphans --build

up: 
	docker-compose up -d --remove-orphans

restart: 
	docker-compose restart "${ARGS}"

stop: 
	docker-compose stop "${ARGS}"

stopall: 
	docker-compose stop

down: 
	docker-compose down --remove-orphans

logs: 
	docker-compose logs --tail 200 -f "${ARGS}"

log: 
	docker-compose logs --tail 200 -f web

makemigrations:
	docker-compose exec web python manage.py makemigrations
	docker-compose exec web python manage.py migrate

migrate:
	docker-compose exec web python manage.py migrate '${ARGS}'


createsuperuser:
	docker-compose exec web python manage.py createsuperuser --no-input --email admin@admin.com --first_name LunaTech --last_name Admin --cellphone 11911111111
poetry:
	docker-compose exec web poetry add '${ARGS}'
	poetry install

install:
	poetry install

poetry_remove:
	docker-compose exec web poetry remove '${ARGS}'
	poetry install

_startapp:
	docker-compose exec web python manage.py startapp '${ARGS}'

startapp: _startapp fix_permissions

restore_database_alias:
	docker-compose exec db psql -d ${POSTGRES_NAME} -U ${POSTGRES_USER} -c "drop schema public cascade; create schema public;"
	docker-compose exec db psql -d ${POSTGRES_NAME} -U ${POSTGRES_USER} -f /docker-entrypoint-initdb.d/backup.sql


restore_database: restore_database_alias makemigrations

load_data:
	docker-compose exec web python manage.py loaddata */fixtures/*.json

test:
	clear
	docker-compose exec web python manage.py test

help:
	docker-compose exec web python manage.py help

sh:
	docker-compose exec "${ARGS}" bash

tasks:
	docker-compose restart tasks

_init:
	cp .env.sample .env

init: _init down build makemigrations createsuperuser fix_permissions

format: isort flake fix_permissions
