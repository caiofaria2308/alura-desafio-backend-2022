from django.contrib import admin

from financeiro.models import Despesa, Receita


@admin.register(Receita)
class ReceitaAdmin(admin.ModelAdmin):
    pass


@admin.register(Despesa)
class DespesaAdmin(admin.ModelAdmin):
    pass
