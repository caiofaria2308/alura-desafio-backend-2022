import django_filters as filters

from financeiro.models import Despesa, Receita


class ReceitaFilter(filters.FilterSet):
    mes = filters.NumberFilter(field_name="data__month", lookup_expr="exact")
    ano = filters.NumberFilter(field_name="data__year", lookup_expr="exact")
    descricao = filters.CharFilter(field_name="descricao", lookup_expr="contains")

    class Meta:
        model = Receita
        fields = ["mes", "ano", "descricao"]


class DespesaFilter(filters.FilterSet):
    mes = filters.NumberFilter(field_name="data__month", lookup_expr="exact")
    ano = filters.NumberFilter(field_name="data__year", lookup_expr="exact")
    descricao = filters.CharFilter(field_name="descricao", lookup_expr="contains")

    class Meta:
        model = Despesa
        fields = ["mes", "ano", "descricao"]
