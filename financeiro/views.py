from rest_framework import generics
from rest_framework.views import Response, status
from rest_framework.viewsets import ModelViewSet

from financeiro.filters import DespesaFilter, ReceitaFilter
from financeiro.models import Despesa, Receita
from financeiro.serializers import (
    DespesaSerializer,
    ReceitaSerializer,
    ResumoDoMeSerializer,
)


class ReceitaViewSet(ModelViewSet):
    serializer_class = ReceitaSerializer
    queryset = Receita.objects.all()
    search_fields = ["descricao"]
    filterset_class = ReceitaFilter


class DespesaViewSet(ModelViewSet):
    serializer_class = DespesaSerializer
    queryset = Despesa.objects.all()
    search_fields = ["descricao"]
    filterset_class = DespesaFilter



class ReceitaMesList(generics.ListAPIView):
    serializer_class = ReceitaSerializer

    def get_queryset(self):
        mes = self.kwargs.get("mes")
        ano = self.kwargs.get("ano")
        return Receita.objects.filter(
            data__year=ano,
            data__month=mes
        )


class DespesaMesList(generics.ListAPIView):
    serializer_class = DespesaSerializer

    def get_queryset(self):
        mes = self.kwargs.get("mes")
        ano = self.kwargs.get("ano")
        return Despesa.objects.filter(
            data__year=ano,
            data__month=mes
        )


class ResumoDoMesView(generics.GenericAPIView):
    serializer_class = ResumoDoMeSerializer
    queryset = Despesa.objects.all()

    def get(self, request, *args, **kwargs):
        mes = self.kwargs.get("mes")
        ano = self.kwargs.get("ano")
        total_receita = 0
        total_despesa = 0
        saldo = 0
        total_despesa_por_categoria = {}
        receitas = Receita.objects.filter(
            data__year=ano,
            data__month=mes
        )
        despesas = Despesa.objects.filter(
            data__year=ano,
            data__month=mes
        )
        receita: Receita
        despesa: Despesa
        for categoria, _ in dict(Despesa.OPCOES_CATEGORIA).items():
            total_despesa_por_categoria[categoria] = 0
        for receita in receitas:
            total_receita += receita.valor
        for despesa in despesas:
            total_despesa += despesa.valor
            total_despesa_por_categoria[despesa.categoria] += despesa.valor
        saldo = total_receita - total_despesa
        return Response({
            "total_receita": total_receita,
            "total_despesa": total_despesa,
            "saldo": saldo,
            "total_despesa_por_categoria": total_despesa_por_categoria
        }, status=status.HTTP_200_OK)
