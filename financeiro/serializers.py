from rest_framework import serializers

from financeiro.models import Despesa, Receita


class ReceitaSerializer(serializers.ModelSerializer):
    data = serializers.DateField(required=True)
    class Meta:
        model = Receita
        fields = "__all__"
        read_only_fields = ["author", "updated_by"]

    def validate(self, attrs):
        attrs: dict = super().validate(attrs)
        exists = Receita.objects.filter(
            descricao=attrs.get("descricao"),
            data__year=attrs.get("data").year,
            data__month=attrs.get("data").month,
        )
        if self.instance:
            exists = exists.exclude(id=self.instance.id)
        if exists:
            raise serializers.ValidationError(
                "Ja existe uma receita com essa descricao para esse mes"
            )
        return attrs


class DespesaSerializer(serializers.ModelSerializer):
    categoria = serializers.ChoiceField(allow_null=True, choices=Despesa.OPCOES_CATEGORIA)
    class Meta:
        model = Despesa
        fields = "__all__"
        read_only_fields = ["author", "updated_by"]

    def validate(self, attrs):
        attrs: dict = super().validate(attrs)
        exists = Receita.objects.filter(
            descricao=attrs.get("descricao"),
            data__year=attrs.get("data").year,
            data__month=attrs.get("data").month,
        )
        if self.instance:
            exists = exists.exclude(id=self.instance.id)
        if exists:
            raise serializers.ValidationError(
                "Ja existe uma despesa com essa descricao para esse mes"
            )
        return attrs


class ResumoDoMeSerializer(serializers.Serializer):
    total_receita = serializers.FloatField()
    total_despesa = serializers.FloatField()
    saldo = serializers.FloatField()
    total_despesa_por_categoria = serializers.DictField()
