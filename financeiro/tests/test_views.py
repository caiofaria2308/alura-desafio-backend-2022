from financeiro.models import Despesa, Receita
from setup.utils import BaseTest
from django.urls import reverse
from rest_framework.views import status

class FinanceiroReceitaTests(BaseTest):
    def test_listagem_receitas(self):
        url = reverse("financeiro-receitas-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get("count"), 0)
        
    def test_cadastro_receitas(self):
        url = reverse("financeiro-receitas-list")
        data = "2020-01-01"
        payload = {
            "descricao": "Salário",
            "valor": 1000.00,
            "data": data
        }
        response = self.client.post(url, payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Receita.objects.count(), 1)
    
    def test_selecionar_receita(self):
        data = "2020-01-01"
        receita: Receita = Receita.objects.create(
            descricao="Salário",
            valor=1000.00,
            data=data
        )
        url = reverse("financeiro-receitas-detail", kwargs={"pk": receita.pk})
        esperado = {
            "id": str(receita.pk),
            "descricao": receita.descricao,
            "valor": receita.valor,
            "data": receita.data
        }
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        recebido: dict = response.json()
        recebido.pop("author")
        recebido.pop("updated_by")
        recebido.pop("created_at")
        recebido.pop("updated_at")
        self.assertDictEqual(recebido, esperado)   
    
    def test_atualizar_receita(self):
        data = "2020-01-01"
        receita: Receita = Receita.objects.create(
            descricao="Salário",
            valor=1000.00,
            data=data
        )
        url = reverse("financeiro-receitas-detail", kwargs={"pk": receita.pk})
        esperado = {
            "id": str(receita.pk),
            "descricao": receita.descricao,
            "valor": receita.valor,
            "data": receita.data
        }
        atualizar = esperado
        atualizar.update({"descricao": "Salário atualizado", "valor": 1200.00})
        response = self.client.patch(url, atualizar, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        recebido: dict = response.json()
        recebido.pop("author")
        recebido.pop("updated_by")
        recebido.pop("created_at")
        recebido.pop("updated_at")
        self.assertDictEqual(recebido, atualizar)

    def test_remover_receita(self):
        data = "2020-01-01"
        receita: Receita = Receita.objects.create(
            descricao="Salário",
            valor=1000.00,
            data=data
        )
        url = reverse("financeiro-receitas-detail", kwargs={"pk": receita.pk})
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        
        
class FinanceiroDespesaTests(BaseTest):
    def test_listagem_despesas(self):
        url = reverse("financeiro-despesas-list")
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json().get("count"), 0)
        
    def test_cadastro_despesas(self):
        url = reverse("financeiro-despesas-list")
        data = "2020-01-01"
        payload = {
            "descricao": "Mercado",
            "valor": 1000.00,
            "data": data,
            "categoria": "Alimentação"
        }
        response = self.client.post(url, payload, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Despesa.objects.count(), 1)
    
    def test_selecionar_despesa(self):
        data = "2020-01-01"
        despesa: Despesa = Despesa.objects.create(
            descricao="Mercado",
            valor=1000.00,
            data=data,
            categoria=Despesa.OPCAO_ALIMENTACAO[0]
        )
        url = reverse("financeiro-despesas-detail", kwargs={"pk": despesa.pk})
        esperado = {
            "id": str(despesa.pk),
            "descricao": despesa.descricao,
            "valor": despesa.valor,
            "data": despesa.data,
            "categoria": "Alimentação"
        }
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        recebido: dict = response.json()
        recebido.pop("author")
        recebido.pop("updated_by")
        recebido.pop("created_at")
        recebido.pop("updated_at")
        self.assertDictEqual(recebido, esperado)   
    
    def test_atualizar_despesa(self):
        data = "2020-01-01"
        despesa: Despesa = Despesa.objects.create(
            descricao="Mercado",
            valor=1000.00,
            data=data,
            categoria=Despesa.OPCAO_ALIMENTACAO[0]
        )
        url = reverse("financeiro-despesas-detail", kwargs={"pk": despesa.pk})
        esperado = {
            "id": str(despesa.pk),
            "descricao": despesa.descricao,
            "valor": despesa.valor,
            "data": despesa.data,
            "categoria": "Alimentação"
        }
        atualizar = esperado
        atualizar.update({"descricao": "Salário atualizado", "valor": 1200.00})
        response = self.client.patch(url, atualizar, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        recebido: dict = response.json()
        recebido.pop("author")
        recebido.pop("updated_by")
        recebido.pop("created_at")
        recebido.pop("updated_at")
        self.assertDictEqual(recebido, atualizar)

    def test_remover_despesa(self):
        data = "2020-01-01"
        despesa: Despesa = Despesa.objects.create(
            descricao="Salário",
            valor=1000.00,
            data=data,
            categoria=Despesa.OPCAO_ALIMENTACAO[0]
        )
        url = reverse("financeiro-despesas-detail", kwargs={"pk": despesa.pk})
        response = self.client.delete(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)        
