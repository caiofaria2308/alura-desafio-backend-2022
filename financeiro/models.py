from author.decorators import with_author
from django.core.validators import MinValueValidator
from django.db import models

from setup.utils import BaseModel


@with_author
class Receita(BaseModel):
    descricao = models.CharField(verbose_name="Descricao", max_length=256)
    valor = models.FloatField(verbose_name="Valor", validators=[MinValueValidator(0)])
    data = models.DateField(verbose_name="Data")

    class Meta:
        verbose_name = "Receita"
        verbose_name_plural = "Receitas"


@with_author
class Despesa(BaseModel):
    OPCAO_ALIMENTACAO = ("Alimentação", "Alimentação")
    OPCAO_SAUDE = ("Saúde", "Saúde")
    OPCAO_MORADIA = ("Moradia", "Moradia")
    OPCAO_TRANSPORTE = ("Transporte", "Transporte")
    OPCAO_EDUCACAO = ("Educação", "Educação")
    OPCAO_LAZER = ("Lazer", "Lazer")
    OPCAO_IMPREVISTOS = ("Imprevistos", "Imprevistos")
    OPCAO_OUTRAS = ("Outras", "Outras")
    OPCOES_CATEGORIA = (
        OPCAO_ALIMENTACAO,
        OPCAO_SAUDE,
        OPCAO_MORADIA,
        OPCAO_TRANSPORTE,
        OPCAO_EDUCACAO,
        OPCAO_LAZER,
        OPCAO_IMPREVISTOS,
        OPCAO_OUTRAS
    )
    descricao = models.CharField(verbose_name="Descricao", max_length=256)
    categoria = models.CharField(verbose_name="Categoria", max_length=64, choices=OPCOES_CATEGORIA, default=OPCAO_OUTRAS[0])
    valor = models.FloatField(verbose_name="Valor", validators=[MinValueValidator(0)])
    data = models.DateField(verbose_name="Data")

    class Meta:
        verbose_name = "Despesa"
        verbose_name_plural = "Despesas"
