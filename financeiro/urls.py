from django.urls import path
from rest_framework.routers import DefaultRouter

from financeiro.views import (
    DespesaMesList,
    DespesaViewSet,
    ReceitaMesList,
    ReceitaViewSet,
    ResumoDoMesView,
)

router = DefaultRouter()
router.register(r"receitas", ReceitaViewSet, basename="financeiro-receitas")
router.register(r"despesas", DespesaViewSet, basename="financeiro-despesas")
urlpatterns = router.urls

urlpatterns += [
    path("receitas/<int:ano>/<int:mes>", ReceitaMesList.as_view(), name="financeiro-lista-receitas-por-mes"),
    path("despesas/<int:ano>/<int:mes>", DespesaMesList.as_view(), name="financeiro-lista-despesas-por-mes"),
    path("resumo/<int:ano>/<int:mes>", ResumoDoMesView.as_view(), name="financeiro-resumo-do-mes"),
]
