from django.urls import path

from account import views

urlpatterns = [
    path("new-user/", views.NewUserCreate.as_view()),
    path("users/", views.UserViewSet.as_view()),
    path("user/get-me/", views.UserMeView.as_view()),
    path("user/<int:pk>/update-password/", views.UserUpdatePasswordViewSet.as_view()),
    path("user/<int:pk>", views.UserList.as_view()),
]
