
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group, Permission
from django.contrib.sites.shortcuts import get_current_site
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from rest_framework import serializers

from setup.token import account_activation_token

User = get_user_model()


class GroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = Group
        fields = "__all__"


class UserSerializer(serializers.ModelSerializer):
    groups = GroupSerializer(many=True, read_only=True)
    password = serializers.CharField(write_only=True)
    is_trusty = serializers.ReadOnlyField()
    last_login = serializers.ReadOnlyField()
    

    class Meta:
        model = User
        exclude = ["is_staff", "is_active", "is_superuser", "user_permissions"]

    def create(self, validated_data: dict):
        user: User = super().create(validated_data)
        current_site = get_current_site(self.context["request"])
        mail_subject = "Activation link has been sent to your email id"
        user.set_password(validated_data.get("password"))
        user.is_active = False
        user.save()
        if validated_data.get("address_zip_code"):
            user.update_address(validated_data.get("address_zip_code"))
            user = User.objects.get(pk=user.pk)
        message = render_to_string(
            "acc_active_email.html",
            {
                "user": user,
                "domain": current_site.domain,
                "uid": urlsafe_base64_encode(force_bytes(user.pk)),
                "token": account_activation_token.make_token(user),
            },
        )
        user.email_user(mail_subject, message)
        permissions = Permission.objects.filter(
            name__in = [
                "Can add Despesa",
                "Can change Despesa",
                "Can view Despesa",
                "Can delete Despesa",
                "Can add Receita",
                "Can change Receita",
                "Can view Receita",
                "Can delete Receita",
                "Can change User",
                "Can view User",
            ]
        )
        for permission in permissions:
            user.user_permissions.add(permission)
        return user        
    

    def update(self, instance: User, validated_data: dict):
        super().update(instance, validated_data)
        user = self.context["request"].user
        if user.pk != instance.pk and not (user.is_staff):
            raise serializers.ValidationError({"detail": "Você não tem permissão para executar essa ação"})
        if validated_data.get("address_zip_code"):
            instance.update_address(validated_data.get("address_zip_code"))
            instance = User.objects.get(pk=instance.pk)
        return instance


class UserUpdatePasswordSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(write_only=True)
    new_password = serializers.CharField(write_only=True)
    confirm_password = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ["id", "old_password", "new_password", "confirm_password"]

    def update(self, instance: User, validated_data: dict):
        user = self.context["request"].user
        if user.pk != instance.pk and not (user.is_staff):
            raise serializers.ValidationError({"detail": "Você não tem permissão para executar essa ação"})
        instance.set_password(validated_data.get("new_password"))
        instance.save()
        return instance

    def validate(self, attrs):
        data: dict = super().validate(attrs)
        user: User = User.objects.get(pk=self.instance.pk)
        if user.check_password(data.get("old_password")):
            if data.get("new_password") == data.get("confirm_password"):
                return attrs
            else:
                raise serializers.ValidationError(
                    detail="Nova senha não combina com a confirmação"
                )
        else:
            raise serializers.ValidationError(
                detail="Senha antiga não é válida")
   
