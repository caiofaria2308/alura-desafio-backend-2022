from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)
from django.core.mail import send_mail
from django.db import models
from django.utils import timezone

from setup.settings import DEFAULT_FROM_EMAIL
from setup.utils import get_address_by_zip_code


class UserManager(BaseUserManager):
    def _create_user(
        self,
        cellphone,
        first_name,
        last_name,
        email,
        password,
        is_staff,
        is_superuser,
        **extra_fields
    ):
        now = timezone.now()
        if not cellphone:
            raise ValueError("The given cellphone must be set")
        email = self.normalize_email(email)
        user = self.model(
            cellphone=cellphone,
            first_name=first_name,
            last_name=last_name,
            email=email,
            is_staff=is_staff,
            is_active=True,
            is_superuser=is_superuser,
            last_login=now,
            date_joined=now,
            **extra_fields
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(
        self,
        cellphone,
        first_name,
        last_name,
        email=None,
        password=None,
        **extra_fields
    ):
        return self._create_user(
            cellphone,
            first_name,
            last_name,
            email,
            password,
            False,
            False,
            **extra_fields
        )

    def create_superuser(
        self, cellphone, first_name, last_name, email, password, **extra_fields
    ):
        user = self._create_user(
            cellphone,
            first_name,
            last_name,
            email,
            password,
            True,
            True,
            **extra_fields
        )
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser, PermissionsMixin):
    first_name = models.CharField("first name", max_length=32)
    last_name = models.CharField("last name", max_length=32)
    cellphone = models.CharField(
        "Cellphone number", max_length=11, unique=True)
    email = models.EmailField("email address", max_length=255, unique=True)
    address_zip_code = models.CharField(
        "Zip code", max_length=9, null=True, blank=True)
    address_address = models.CharField(
        "Address", max_length=256, null=True, blank=True)
    address_number = models.CharField(
        "Address number", max_length=32, null=True, blank=True
    )
    address_country = models.CharField(
        "Address country", max_length=128, null=True, blank=True
    )
    address_state = models.CharField(
        "Address state", max_length=2, null=True, blank=True
    )
    address_city = models.CharField(
        "Address city", max_length=128, null=True, blank=True
    )
    address_district = models.CharField(
        "Address district", max_length=128, null=True, blank=True
    )
    address_complement = models.CharField(
        "Address complement", max_length=512, null=True, blank=True
    )

    is_staff = models.BooleanField(
        "staff status",
        default=False,
        help_text="Designates whether the user can log into this admin site.",
    )
    is_active = models.BooleanField(
        "active",
        default=True,
        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
    )
    is_trusty = models.BooleanField(
        "Trusty",
        default=False,
        help_text="Designates whether this user has confirmed his account.",
    )

    date_joined = models.DateTimeField("date joined", auto_now_add=True)

    USERNAME_FIELD = "cellphone"
    REQUIRED_FIELDS = ["first_name", "last_name", "email"]

    objects = UserManager()

    class Meta:
        verbose_name = "User"
        verbose_name_plural = "Users"

    def get_full_name(self):
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        return self.first_name

    def email_user(self, subject, message, from_email=DEFAULT_FROM_EMAIL):
        try:
            send_mail(subject, message, from_email, [self.email])
        except Exception as e:
            print(e)

    def update_address(self, zip_code):
        try:
            address: dict = get_address_by_zip_code(zip_code)
            User.objects.filter(pk=self.pk).update(**address)
        except Exception as e:
            print(e)
