from django.contrib import admin

from account.models import User

from .forms import UserAdminChangeForm, UserAdminCreationForm


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm
    list_display = [
        "id",
        "cellphone",
        "first_name",
        "last_name",
        "is_active",
        "is_staff",
        "is_trusty"
    ]
    search_fields = [
        "cellphone",
        "email",
        "first_name"
    ]

    list_filter = [
        "is_staff",
        "is_active",
        "is_trusty"
    ]

    readonly_fields = ('last_login', 'date_joined',)
