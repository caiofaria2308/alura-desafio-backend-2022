from django.http import HttpResponse
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from rest_framework import generics, permissions

from account.serializers import UserSerializer, UserUpdatePasswordSerializer
from setup.token import account_activation_token

from .models import User


class UserViewSet(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    search_fields = [
        "^cellphone",
        "^email",
        "$first_name",
    ]
    ordering_fields = ["id", "first_name", "updated_at"]
    queryset = User.objects.prefetch_related("groups").all()


class UserList(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.prefetch_related("groups").all()


class UserMeView(generics.ListAPIView):
    serializer_class = UserSerializer

    def get_queryset(self):
        return User.objects.prefetch_related("groups").filter(pk=self.request.user.pk)


class UserUpdatePasswordViewSet(generics.UpdateAPIView):
    serializer_class = UserUpdatePasswordSerializer
    queryset = User.objects.all()


def activate(request, uidb64, token):
    try:
        uid = force_str(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.is_trusty = True
        user.save()
        return HttpResponse(
            "Thank you for your email confirmation. Now you can login your account."
        )
    else:
        return HttpResponse("Activation link is invalid!")


class NewUserCreate(generics.CreateAPIView):
    serializer_class = UserSerializer
    queryset = User.objects.all()
    permission_classes = [permissions.AllowAny]