
# Alura desafio backend 2022

Desafio da alura para criação de uma API de controle de gastos/receitas.


# Alura desafio backend 2022

Desafio da alura para criação de uma API de controle de gastos/receitas.


## Instalação


#### * É necessário ter o docker e o docker-compose instalado.
```bash
  $ make init
```

## Iniciar serviço
```bash
    $ make up
```

## Testes automatizados
```bash
    $ make test
```


    
## Documentação

[Documentação da api](http://localhost:8000/api/swagger/)



## Autores

- [@caiofaria2308](https://www.gitlab.com/caiofaria2308)

